# Sheet Prototyping

<img src='img/ur-rotation-end-effector.png' height=400px>
<img src='img/wiz-and-rotary.jpg' height=400px>

Cutting and folding sheets may be the fastest way to put precision features on multiple planes.  A sheet fabricated prototype can go from design to part in minutes and scale to massive production volumes.  The pictures above show a robot arm end effector holding a motor and transmission, while the pictures below show a custom shipping box.

<img src='img/cardboard-1.jpg' height=200px>
<img src='img/cardboard-2.jpg' height=200px>
<img src='img/cardboard-3.jpg' height=200px>

Don't take it from me, listen to the master, Dan Gelbart, in his series "Making Prototypes":
https://www.youtube.com/watch?v=dS5kwdaNhZo


### Materials

* Cardboard
* Paper
* <a href='http://www.itwformex.com/'>Polypropylene</a>
* <a href='https://www.mcmaster.com/#standard-plastic-sheets/=1a6mf16'>Dibond</a>
* Steel
* Stainless Steel
* Aluminum: Must be a formable series, like 1100 or <a href='https://www.onlinemetals.com/merchant.cfm?id=753&step=2&top_cat=60'>3003</a>

### Cutting Tools

* <a href='http://cba.mit.edu/tools/display/?type=tool&id=universal_laser_x2600_laser_cutter#'>Laser cutters</a>
* <a href='http://cba.mit.edu/tools/display/?type=tool&id=omax_2652_waterjet_cutter#'>Waterjet</a>
* <a href='http://cba.mit.edu/tools/display/?type=tool&id=zund_g3_l-2500_digital_cutter#'>ZUND</a>


### Creasing Methods

<img src='img/press-brake-1.jpg' height=300px>
<img src='img/press-brake-2.jpg' height=300px>

* Press Brake
* <a href='http://cba.mit.edu/tools/display/?type=tool&id=finger_brake#'>Finger Brake</a>
* <a href='http://www.bbc.co.uk/staticarchive/c674c7d0cbfb7d03e3aacf3f50125a82ff4ed61b.gif'>Vice Bending</a>
* Perforating
* Half-cutting
* Bevel routing
* Bead creasing
* Roller
<img src='img/mmvv-sequence-small.mp4' width=800px> 

### Closing seams

* welding
* spot welding
* <a href='https://www.mcmaster.com/#rivets/=1a6o1ew'>rivets</a>
* <a href='https://www.mcmaster.com/#inserts-for-metals/=1a6o163'>sheet metal inserts</a>
* tabs and slots
* adhesives


### Design Factors

<img src='img/k-factor.png' width=300px>

* Thickness
* <a href='https://www.hawkridgesys.com/blog/sheet-metal-understanding-k-factor/'>K-Factor</a>
* <a href='http://files.engineering.com/download.aspx?folder=b0283dfb-f6a8-4c7c-8207-eb6510b27548&file=Sheet_Metal_Design_Considerations.pdf'>Relief</a>

### Design tools

<img src='img/fusion-sheet-metal.png' width=300px>

* Fusion 360 sheet metal: 
	* <a href=' sheet-metal-test2 v4.f3d'>Demo project from the recitation</a>
	* <a href='https://www.youtube.com/watch?v=nEjFMYNGY4g'>tutorial (nyc cnc)</a>
	* <a href='https://www.youtube.com/watch?v=1aDN44__L9E'>tutorial (cardboard)</a>
	* <a href='https://www.youtube.com/watch?v=UXNSlxXjzhk'>sheet metal gems part 1</a>
	* <a href='https://www.youtube.com/watch?v=_CPkUqedqhA'>sheet metal gems part 2</a>
* <a href='http://docs.mcneel.com/rhino/5/help/en-us/commands/unrollsrf.htm'>Rhinoceros 'UnrollSrf' Command</a> to get <a href='http://www.instructables.com/id/Giant-Inflatable-Robot/'>patches</a>
* <a href='http://www.solidworks.com/sw/products/3d-cad/sheet-metal-design.htm'>Solidworks sheet metal</a>
* 123D Make (deprecated...)
* <a href='https://www.exactflat.com/'>ExactFlat</a>